<?php
/**
 * Created by PhpStorm.
 * User: Kalvot
 * Date: 14.06.2019
 * Time: 18:38
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="app_dashboard")
     */
    public function dashboard()
    {
        return $this->render("dashboard/dashboard.html.twig");
    }
}