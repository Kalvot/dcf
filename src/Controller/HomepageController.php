<?php
/**
 * Created by PhpStorm.
 * User: Kalvot
 * Date: 14.06.2019
 * Time: 18:38
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage")
     */
    public function homepage()
    {
        return $this->render("base.html.twig");
    }
}