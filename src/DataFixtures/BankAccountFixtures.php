<?php
/**
 * Created by PhpStorm.
 * User: Kalvot
 * Date: 09.07.2019
 * Time: 11:43
 */

namespace App\DataFixtures;


use App\Entity\BankAccount;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class BankAccountFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $acc = new BankAccount();
        $acc->setUser($this->getReference(UserFixtures::ADMIN_USER_REF));
        $acc->setBank($this->getReference(BankFixtures::ALIOR_BANK_REF));
        $acc->setNumber('CCAAAAAAAABBBBBBBBBBBBBBBB');

        $manager->persist($acc);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            BankFixtures::class,
        );
    }
}