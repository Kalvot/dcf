<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public const ADMIN_USER_REF = 'admin-user';

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail("kalvot@gmail.com");
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword('$argon2i$v=19$m=65536,t=6,p=1$TBUimiynqZYDNMZKtQ4BRA$5WA+PpMiw0MxZ3RpW15OSdqh3eT8Ur13s04/UVRDCAM');

        $manager->persist($user);
        $manager->flush();

        $this->addReference(self::ADMIN_USER_REF, $user);
    }
}
