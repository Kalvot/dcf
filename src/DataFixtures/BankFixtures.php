<?php
/**
 * Created by PhpStorm.
 * User: Kalvot
 * Date: 09.07.2019
 * Time: 12:05
 */

namespace App\DataFixtures;


use App\Entity\Bank;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BankFixtures extends Fixture
{
    public const ALIOR_BANK_REF = 'alior-bank';

    public function load(ObjectManager $manager)
    {
        $alior = new Bank('Alior Bank', '');
        $santander = new Bank('Santander', '');

        $manager->persist($alior);
        $manager->persist($santander);
        $manager->flush();

        $this->addReference(self::ALIOR_BANK_REF, $alior);
    }
}