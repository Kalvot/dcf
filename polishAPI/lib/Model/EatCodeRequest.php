<?php
/**
 * EatCodeRequest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Polish API
 *
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.5
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * EatCodeRequest Class Doc Comment
 *
 * @category Class
 * @description Klasa zapytania o uzyskanie kodu autoryzacyjnego zgodnego z OAuth2 na podstawie przekazanego kodu jednorazowego, wygenerowanego w EAT
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class EatCodeRequest implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'EatCodeRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'request_header' => '\Swagger\Client\Model\RequestHeaderWithoutTokenCallbackAS',
        'response_type' => 'string',
        'eat_code' => 'string',
        'eat_type' => '\Swagger\Client\Model\DictionaryItem',
        'client_id' => 'string',
        'scope' => 'string',
        'scope_details' => '\Swagger\Client\Model\ScopeDetailsInput',
        'state' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'request_header' => null,
        'response_type' => null,
        'eat_code' => null,
        'eat_type' => null,
        'client_id' => null,
        'scope' => null,
        'scope_details' => null,
        'state' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'request_header' => 'requestHeader',
        'response_type' => 'response_type',
        'eat_code' => 'eatCode',
        'eat_type' => 'eatType',
        'client_id' => 'client_id',
        'scope' => 'scope',
        'scope_details' => 'scope_details',
        'state' => 'state'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'request_header' => 'setRequestHeader',
        'response_type' => 'setResponseType',
        'eat_code' => 'setEatCode',
        'eat_type' => 'setEatType',
        'client_id' => 'setClientId',
        'scope' => 'setScope',
        'scope_details' => 'setScopeDetails',
        'state' => 'setState'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'request_header' => 'getRequestHeader',
        'response_type' => 'getResponseType',
        'eat_code' => 'getEatCode',
        'eat_type' => 'getEatType',
        'client_id' => 'getClientId',
        'scope' => 'getScope',
        'scope_details' => 'getScopeDetails',
        'state' => 'getState'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['request_header'] = isset($data['request_header']) ? $data['request_header'] : null;
        $this->container['response_type'] = isset($data['response_type']) ? $data['response_type'] : null;
        $this->container['eat_code'] = isset($data['eat_code']) ? $data['eat_code'] : null;
        $this->container['eat_type'] = isset($data['eat_type']) ? $data['eat_type'] : null;
        $this->container['client_id'] = isset($data['client_id']) ? $data['client_id'] : null;
        $this->container['scope'] = isset($data['scope']) ? $data['scope'] : null;
        $this->container['scope_details'] = isset($data['scope_details']) ? $data['scope_details'] : null;
        $this->container['state'] = isset($data['state']) ? $data['state'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['request_header'] === null) {
            $invalidProperties[] = "'request_header' can't be null";
        }
        if ($this->container['response_type'] === null) {
            $invalidProperties[] = "'response_type' can't be null";
        }
        if ($this->container['eat_code'] === null) {
            $invalidProperties[] = "'eat_code' can't be null";
        }
        if ($this->container['client_id'] === null) {
            $invalidProperties[] = "'client_id' can't be null";
        }
        if ($this->container['scope'] === null) {
            $invalidProperties[] = "'scope' can't be null";
        }
        if ($this->container['scope_details'] === null) {
            $invalidProperties[] = "'scope_details' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets request_header
     *
     * @return \Swagger\Client\Model\RequestHeaderWithoutTokenCallbackAS
     */
    public function getRequestHeader()
    {
        return $this->container['request_header'];
    }

    /**
     * Sets request_header
     *
     * @param \Swagger\Client\Model\RequestHeaderWithoutTokenCallbackAS $request_header request_header
     *
     * @return $this
     */
    public function setRequestHeader($request_header)
    {
        $this->container['request_header'] = $request_header;

        return $this;
    }

    /**
     * Gets response_type
     *
     * @return string
     */
    public function getResponseType()
    {
        return $this->container['response_type'];
    }

    /**
     * Sets response_type
     *
     * @param string $response_type Typ odpowiedzi. Wartość stała: code
     *
     * @return $this
     */
    public function setResponseType($response_type)
    {
        $this->container['response_type'] = $response_type;

        return $this;
    }

    /**
     * Gets eat_code
     *
     * @return string
     */
    public function getEatCode()
    {
        return $this->container['eat_code'];
    }

    /**
     * Sets eat_code
     *
     * @param string $eat_code Jednorazowy kod autoryzacyjny wygenerowany przez EAT
     *
     * @return $this
     */
    public function setEatCode($eat_code)
    {
        $this->container['eat_code'] = $eat_code;

        return $this;
    }

    /**
     * Gets eat_type
     *
     * @return \Swagger\Client\Model\DictionaryItem
     */
    public function getEatType()
    {
        return $this->container['eat_type'];
    }

    /**
     * Sets eat_type
     *
     * @param \Swagger\Client\Model\DictionaryItem $eat_type eat_type
     *
     * @return $this
     */
    public function setEatType($eat_type)
    {
        $this->container['eat_type'] = $eat_type;

        return $this;
    }

    /**
     * Gets client_id
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->container['client_id'];
    }

    /**
     * Sets client_id
     *
     * @param string $client_id Identyfikator TPP
     *
     * @return $this
     */
    public function setClientId($client_id)
    {
        $this->container['client_id'] = $client_id;

        return $this;
    }

    /**
     * Gets scope
     *
     * @return string
     */
    public function getScope()
    {
        return $this->container['scope'];
    }

    /**
     * Sets scope
     *
     * @param string $scope Typ zgody, o którą prosi TPP. Wybrana wartość z listy dostępnych identyfikatorów zgód, opisanych w specyfikacji standardu Polish API.
     *
     * @return $this
     */
    public function setScope($scope)
    {
        $this->container['scope'] = $scope;

        return $this;
    }

    /**
     * Gets scope_details
     *
     * @return \Swagger\Client\Model\ScopeDetailsInput
     */
    public function getScopeDetails()
    {
        return $this->container['scope_details'];
    }

    /**
     * Sets scope_details
     *
     * @param \Swagger\Client\Model\ScopeDetailsInput $scope_details scope_details
     *
     * @return $this
     */
    public function setScopeDetails($scope_details)
    {
        $this->container['scope_details'] = $scope_details;

        return $this;
    }

    /**
     * Gets state
     *
     * @return string
     */
    public function getState()
    {
        return $this->container['state'];
    }

    /**
     * Sets state
     *
     * @param string $state Losowa, unikalna w ramach TPP wartość – zabezpieczenie przed atakiem Cross-Site Request Forgery
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->container['state'] = $state;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


