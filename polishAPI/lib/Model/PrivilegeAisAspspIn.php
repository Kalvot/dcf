<?php
/**
 * PrivilegeAisAspspIn
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Polish API
 *
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.5
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * PrivilegeAisAspspIn Class Doc Comment
 *
 * @category Class
 * @description Lista atrybutów uprawnienia usługi AIS będących przedmiotem zapytania o zgodę PSU / The list of attributes of the privilege of the AIS service that are the subject of the request for PSU&#39;s consent
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PrivilegeAisAspspIn implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'PrivilegeAisAspspIn';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'scope_usage_limit' => 'string',
        'max_allowed_history_long' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'scope_usage_limit' => null,
        'max_allowed_history_long' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'scope_usage_limit' => 'scopeUsageLimit',
        'max_allowed_history_long' => 'maxAllowedHistoryLong'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'scope_usage_limit' => 'setScopeUsageLimit',
        'max_allowed_history_long' => 'setMaxAllowedHistoryLong'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'scope_usage_limit' => 'getScopeUsageLimit',
        'max_allowed_history_long' => 'getMaxAllowedHistoryLong'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const SCOPE_USAGE_LIMIT_SINGLE = 'single';
    const SCOPE_USAGE_LIMIT_MULTIPLE = 'multiple';
    

    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getScopeUsageLimitAllowableValues()
    {
        return [
            self::SCOPE_USAGE_LIMIT_SINGLE,
            self::SCOPE_USAGE_LIMIT_MULTIPLE,
        ];
    }
    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['scope_usage_limit'] = isset($data['scope_usage_limit']) ? $data['scope_usage_limit'] : null;
        $this->container['max_allowed_history_long'] = isset($data['max_allowed_history_long']) ? $data['max_allowed_history_long'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        $allowedValues = $this->getScopeUsageLimitAllowableValues();
        if (!is_null($this->container['scope_usage_limit']) && !in_array($this->container['scope_usage_limit'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'scope_usage_limit', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        if ($this->container['max_allowed_history_long'] === null) {
            $invalidProperties[] = "'max_allowed_history_long' can't be null";
        }
        if (($this->container['max_allowed_history_long'] > 1460)) {
            $invalidProperties[] = "invalid value for 'max_allowed_history_long', must be smaller than or equal to 1460.";
        }

        if (($this->container['max_allowed_history_long'] < 1)) {
            $invalidProperties[] = "invalid value for 'max_allowed_history_long', must be bigger than or equal to 1.";
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets scope_usage_limit
     *
     * @return string
     */
    public function getScopeUsageLimit()
    {
        return $this->container['scope_usage_limit'];
    }

    /**
     * Sets scope_usage_limit
     *
     * @param string $scope_usage_limit Rodzaj limitu zgody / Type of limit of usages
     *
     * @return $this
     */
    public function setScopeUsageLimit($scope_usage_limit)
    {
        $allowedValues = $this->getScopeUsageLimitAllowableValues();
        if (!is_null($scope_usage_limit) && !in_array($scope_usage_limit, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'scope_usage_limit', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['scope_usage_limit'] = $scope_usage_limit;

        return $this;
    }

    /**
     * Gets max_allowed_history_long
     *
     * @return int
     */
    public function getMaxAllowedHistoryLong()
    {
        return $this->container['max_allowed_history_long'];
    }

    /**
     * Sets max_allowed_history_long
     *
     * @param int $max_allowed_history_long Ilość dni wstecz, liczona od momentu wysłania przez TPP żądania o pobranie historii transakcji rachunku płatnicznego, jaką może obejmować odpowiedź na to żądanie / How much account's transaction history is allowed to retrieve in days. This value should be used for calculation based on date of sending the request.
     *
     * @return $this
     */
    public function setMaxAllowedHistoryLong($max_allowed_history_long)
    {

        if (($max_allowed_history_long > 1460)) {
            throw new \InvalidArgumentException('invalid value for $max_allowed_history_long when calling PrivilegeAisAspspIn., must be smaller than or equal to 1460.');
        }
        if (($max_allowed_history_long < 1)) {
            throw new \InvalidArgumentException('invalid value for $max_allowed_history_long when calling PrivilegeAisAspspIn., must be bigger than or equal to 1.');
        }

        $this->container['max_allowed_history_long'] = $max_allowed_history_long;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


