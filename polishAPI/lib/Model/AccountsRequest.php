<?php
/**
 * AccountsRequest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Polish API
 *
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.5
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * AccountsRequest Class Doc Comment
 *
 * @category Class
 * @description Klasa zapytania o rachunki / Accounts Request Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AccountsRequest implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'AccountsRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'request_header' => '\Swagger\Client\Model\RequestHeaderAISCallback',
        'type_of_psu_relation' => 'string',
        'page_id' => 'string',
        'per_page' => 'int'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'request_header' => null,
        'type_of_psu_relation' => null,
        'page_id' => null,
        'per_page' => 'int32'
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'request_header' => 'requestHeader',
        'type_of_psu_relation' => 'typeOfPsuRelation',
        'page_id' => 'pageId',
        'per_page' => 'perPage'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'request_header' => 'setRequestHeader',
        'type_of_psu_relation' => 'setTypeOfPsuRelation',
        'page_id' => 'setPageId',
        'per_page' => 'setPerPage'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'request_header' => 'getRequestHeader',
        'type_of_psu_relation' => 'getTypeOfPsuRelation',
        'page_id' => 'getPageId',
        'per_page' => 'getPerPage'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    const TYPE_OF_PSU_RELATION_OWNER = 'Owner';
    const TYPE_OF_PSU_RELATION_BORROWER = 'Borrower';
    const TYPE_OF_PSU_RELATION_GUARANTOR = 'Guarantor';
    const TYPE_OF_PSU_RELATION_PROXY_OWNER = 'ProxyOwner';
    const TYPE_OF_PSU_RELATION_BENEFICIARY = 'Beneficiary';
    const TYPE_OF_PSU_RELATION_TRUSTEE = 'Trustee';
    

    
    /**
     * Gets allowable values of the enum
     *
     * @return string[]
     */
    public function getTypeOfPsuRelationAllowableValues()
    {
        return [
            self::TYPE_OF_PSU_RELATION_OWNER,
            self::TYPE_OF_PSU_RELATION_BORROWER,
            self::TYPE_OF_PSU_RELATION_GUARANTOR,
            self::TYPE_OF_PSU_RELATION_PROXY_OWNER,
            self::TYPE_OF_PSU_RELATION_BENEFICIARY,
            self::TYPE_OF_PSU_RELATION_TRUSTEE,
        ];
    }
    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['request_header'] = isset($data['request_header']) ? $data['request_header'] : null;
        $this->container['type_of_psu_relation'] = isset($data['type_of_psu_relation']) ? $data['type_of_psu_relation'] : null;
        $this->container['page_id'] = isset($data['page_id']) ? $data['page_id'] : null;
        $this->container['per_page'] = isset($data['per_page']) ? $data['per_page'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['request_header'] === null) {
            $invalidProperties[] = "'request_header' can't be null";
        }
        $allowedValues = $this->getTypeOfPsuRelationAllowableValues();
        if (!is_null($this->container['type_of_psu_relation']) && !in_array($this->container['type_of_psu_relation'], $allowedValues, true)) {
            $invalidProperties[] = sprintf(
                "invalid value for 'type_of_psu_relation', must be one of '%s'",
                implode("', '", $allowedValues)
            );
        }

        if (!is_null($this->container['per_page']) && ($this->container['per_page'] < 1)) {
            $invalidProperties[] = "invalid value for 'per_page', must be bigger than or equal to 1.";
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets request_header
     *
     * @return \Swagger\Client\Model\RequestHeaderAISCallback
     */
    public function getRequestHeader()
    {
        return $this->container['request_header'];
    }

    /**
     * Sets request_header
     *
     * @param \Swagger\Client\Model\RequestHeaderAISCallback $request_header request_header
     *
     * @return $this
     */
    public function setRequestHeader($request_header)
    {
        $this->container['request_header'] = $request_header;

        return $this;
    }

    /**
     * Gets type_of_psu_relation
     *
     * @return string
     */
    public function getTypeOfPsuRelation()
    {
        return $this->container['type_of_psu_relation'];
    }

    /**
     * Sets type_of_psu_relation
     *
     * @param string $type_of_psu_relation Użuwane w celu filtrowania - typ relacji PSU do rachunku / Used for filtering the results - type of relation between PSU and an Account
     *
     * @return $this
     */
    public function setTypeOfPsuRelation($type_of_psu_relation)
    {
        $allowedValues = $this->getTypeOfPsuRelationAllowableValues();
        if (!is_null($type_of_psu_relation) && !in_array($type_of_psu_relation, $allowedValues, true)) {
            throw new \InvalidArgumentException(
                sprintf(
                    "Invalid value for 'type_of_psu_relation', must be one of '%s'",
                    implode("', '", $allowedValues)
                )
            );
        }
        $this->container['type_of_psu_relation'] = $type_of_psu_relation;

        return $this;
    }

    /**
     * Gets page_id
     *
     * @return string
     */
    public function getPageId()
    {
        return $this->container['page_id'];
    }

    /**
     * Sets page_id
     *
     * @param string $page_id Używane w celu stronicowania danych: identyfikator strony, która ma zostać zwrócona w odpowiedzi / Used for paging the results. Identifier of the page to be returned in the response.
     *
     * @return $this
     */
    public function setPageId($page_id)
    {
        $this->container['page_id'] = $page_id;

        return $this;
    }

    /**
     * Gets per_page
     *
     * @return int
     */
    public function getPerPage()
    {
        return $this->container['per_page'];
    }

    /**
     * Sets per_page
     *
     * @param int $per_page Używane w celu stronicowania danych: wielkość strony danych / Page size (paging info)
     *
     * @return $this
     */
    public function setPerPage($per_page)
    {

        if (!is_null($per_page) && ($per_page < 1)) {
            throw new \InvalidArgumentException('invalid value for $per_page when calling AccountsRequest., must be bigger than or equal to 1.');
        }

        $this->container['per_page'] = $per_page;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


