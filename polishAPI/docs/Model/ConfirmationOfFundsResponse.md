# ConfirmationOfFundsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**funds_available** | **bool** | Status - Czy środki są dostępne / Status - are funds available | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


