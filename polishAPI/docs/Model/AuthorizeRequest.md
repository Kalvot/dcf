# AuthorizeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderWithoutTokenAS**](RequestHeaderWithoutTokenAS.md) |  | 
**response_type** | **string** | Typ odpowiedzi. Wartość stała: code | 
**client_id** | **string** | Identyfikator TPP | 
**redirect_uri** | **string** | Adres usługi TPP, na które zostanie wykonane przekierowanie po uwierzytelnieniu klienta ASPSP | 
**scope** | **string** | Typ zgody, o którą prosi TPP. Wybrana wartość z listy dostępnych identyfikatorów zgód, opisanych w specyfikacji standardu Polish API. | 
**scope_details** | [**\Swagger\Client\Model\ScopeDetailsInput**](ScopeDetailsInput.md) |  | 
**state** | **string** | Losowa, unikalna w ramach TPP wartość – zabezpieczenie przed atakiem Cross-Site Request Forgery | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


