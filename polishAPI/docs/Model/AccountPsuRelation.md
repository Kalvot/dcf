# AccountPsuRelation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_of_relation** | **string** | Typ relacji PSU do rachunku / Type of relation between PSU and an Account | 
**type_of_proxy** | **string** | Typ pełnomocnictwa PSU do rachunku płatniczego. Wymagane gdy typ relacji to Pełnomocnik na rachunku. / Type of proxy. Required for ProxyOwner being a type of PSU relation. | [optional] 
**stake** | **int** | Wartość całkowita, wyrażona w procentach, określająca udział PSU w środkach na rachunku lub jego odpowiedzialności w przypadku produktów kredytowych. Dotyczy relacji Owner, Borrower, Guarantor. Opcjonalne. / Integer percentage value defining a participation of PSU in liabilities (in case of credit products) or in ownership of funds on the account. Applicable for the following types of PSU relations: Owner, Borrower, Guarantor. Optional. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


