# SenderRecipient

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **string** | Numer konta nadawcy/odbiorcy / Account number | [optional] 
**account_mass_payment** | **string** | Numer wirtualny rachunku odbiorcy w formacie / Virtual account name | [optional] 
**bank** | [**\Swagger\Client\Model\Bank**](Bank.md) |  | [optional] 
**name_address** | [**\Swagger\Client\Model\NameAddress**](NameAddress.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


