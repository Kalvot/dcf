# ResponseHeader

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **string** | Identyfikator żądania otrzymanego od TPP / Identifier of request received from TPP | [optional] 
**send_date** | [**\DateTime**](\DateTime.md) | Data odpowiedzi, format: YYYY-MM-DDThh:mm:ss[.mmm] / Send date | [optional] 
**is_callback** | **bool** | Znacznik określający czy odpowiedz zostanie przekazana w formie wywołania zwrotnego. true - gdy odpowiedz w formie wywołania zwrotnego. Inna dopuszczalna wartość to false. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


