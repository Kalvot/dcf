# PaymentTokenEntry

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_id** | **string** | Identyfikator płatności. Wymagany warunkowo - jeśli TPP otrzymał ten identyfikator od ASPSP. / Identifier of the payment. Conditionally required - in case TPP has received this identifier from ASPSP. | [optional] 
**tpp_transaction_id** | **string** | Identyfikator transakcji nadany przez TPP. Wymagany warunkowo - jeśli TPP nie otrzymał identyfikatora paymentId od ASPSP. / Identifier of transaction established by TPP. Conditionally required - in case TPP has not received paymentId from ASPSP. | [optional] 
**access_token** | **string** | Access token użyty do zainicjowania płatności / Access token used to initiate payment | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


