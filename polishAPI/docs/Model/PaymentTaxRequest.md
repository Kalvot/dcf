# PaymentTaxRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderCallback**](RequestHeaderCallback.md) |  | 
**recipient** | [**\Swagger\Client\Model\RecipientPISTax**](RecipientPISTax.md) |  | 
**sender** | [**\Swagger\Client\Model\SenderPISDomestic**](SenderPISDomestic.md) |  | 
**transfer_data** | [**\Swagger\Client\Model\TransferDataCurrencyRequiredTax**](TransferDataCurrencyRequiredTax.md) |  | 
**us_info** | [**\Swagger\Client\Model\TransactionInfoTax**](TransactionInfoTax.md) |  | [optional] 
**tpp_transaction_id** | **string** | ID transakcji nadany przez TPP / Transaction ID (TPP) | 
**delivery_mode** | **string** | Tryb pilności / Urgency mode | 
**system** | **string** | Droga jaką przelew ma być rozliczony / The way the transfer should be settled | 
**hold** | **bool** | Czy założyć blokadę (w przypadku np. zlecenia przelewu w dniu wolnym) / Indicates if payment should be holded | [optional] 
**execution_mode** | **string** | Tryb realizacji płatności. Nadrzędna informacja decydująca o tym w jakim trybie zostanie zrealizowana płatność. / Payment execution mode. The superior information deciding which mode is to be used to execute payment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


