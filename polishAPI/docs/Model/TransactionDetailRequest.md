# TransactionDetailRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderAIS**](RequestHeaderAIS.md) |  | 
**item_id** | **string** | ID elementu (transakcji lub blokadzie) nadany przez ASPSP / Element (transaction or hold) ID (ASPSP) | 
**account_number** | **string** | Numer konta nadawcy / Sender account number | 
**booking_date** | [**\DateTime**](\DateTime.md) | Data zaksięgowania operacji, YYYY-MM-DDThh:mm:ss[.mmm] / Transaction booking date | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


