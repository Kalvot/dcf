# PaymentInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_id** | **string** | Identyfiaktor płatności / Payment ID | 
**tpp_transaction_id** | **string** | ID transakcji nadany przez TPP / Transaction ID (TPP) | 
**general_status** | [**\Swagger\Client\Model\PaymentStatus**](PaymentStatus.md) |  | 
**detailed_status** | **string** | Szczegółowy status płatności / Detailed payment status | 
**execution_mode** | **string** | Tryb realizacji płatności. Nadrzędna informacja decydująca o tym w jakim trybie zostanie zrealizowana płatność. / Payment execution mode. The superior information deciding which mode is to be used to execute payment. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


