# ItemInfoBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item_id** | **string** | ID elementu (transakcji lub blokadzie) nadany przez ASPSP / Element (transaction or hold) ID (ASPSP) | 
**amount** | **string** | Kwota transakcji / Amount of the transaction | 
**currency** | **string** | Kod ISO waluty transakcji / Currency (ISO) | [optional] 
**description** | **string** | Tytuł transakcji / Description of the transaction | [optional] 
**transaction_type** | **string** | Typ transakcji / Transaction type | [optional] 
**trade_date** | [**\DateTime**](\DateTime.md) | Data operacji, YYYY-MM-DDThh:mm:ss[.mmm] / Date of the operation | [optional] 
**mcc** | **string** | Kod dla każdej transakcji/operacji wykonanej przy użyciu karty / A code of each transaction performed by card | [optional] 
**aux_data** | [**\Swagger\Client\Model\Map**](Map.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


