# ConfirmationOfFundsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderWithoutToken**](RequestHeaderWithoutToken.md) |  | 
**account_number** | **string** | Numer konta / Account number | 
**amount** | **string** | Wielkość środków której dotyczy zaptanie / Amount of the transaction | 
**currency** | **string** | Kod ISO Waluty (waluta transakcji) / Currency of transaction (ISO) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


