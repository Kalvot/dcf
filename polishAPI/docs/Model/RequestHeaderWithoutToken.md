# RequestHeaderWithoutToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_id** | **string** | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1) zgodnym ze standardem RFC 4122 / Request ID using UUID format (Variant 1, Version 1) described in RFC 4122 standard | 
**user_agent** | **string** | Browser agent dla PSU / PSU browser agent | [optional] 
**ip_address** | **string** | Adres IP końcowego urządzenia PSU. Wymagany dla isDirectPsu&#x3D;true. / IP address of PSU&#39;s terminal device. Required when isDirectPsu&#x3D;true. | [optional] 
**send_date** | [**\DateTime**](\DateTime.md) | Oryginalna data wysłania, format: YYYY-MM-DDThh:mm:ss[.mmm] / Send date | [optional] 
**tpp_id** | **string** | Identyfikator TPP / TPP ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


