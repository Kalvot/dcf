# ScopeDetailsInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**privilege_list** | [**\Swagger\Client\Model\ScopeDetailsInputPrivilegeList[]**](ScopeDetailsInputPrivilegeList.md) | The list of structures describing details of consent. Conditionally required - in case of process of obtaining new consent. | [optional] 
**scope_group_type** | **string** | Type of consent | 
**consent_id** | **string** | Id of consent | 
**scope_time_limit** | [**\DateTime**](\DateTime.md) | Data ważności zgody / consent valid until, YYYY-MM-DDThh:mm:ss[.mmm] | 
**throttling_policy** | **string** | Throttling policy | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


