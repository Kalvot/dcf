# DeleteConsentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderWithoutToken**](RequestHeaderWithoutToken.md) |  | 
**consent_id** | **string** | Identyfikator zgody / Consent ID | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


