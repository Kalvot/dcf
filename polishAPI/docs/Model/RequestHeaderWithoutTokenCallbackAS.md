# RequestHeaderWithoutTokenCallbackAS

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**callback_url** | **string** | adres funkcji zwrotnej / callback URL | [optional] 
**api_key** | **string** | API key dla wywołania funkcji zwrotnej / callback API key | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


