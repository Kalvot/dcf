# AuthorizeResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**aspsp_redirect_uri** | **string** | Adres po stronie ASPSP, na który TPP powinien dokonać przekierowania przeglądarki PSU w celu jego uwierzytelnienia | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


