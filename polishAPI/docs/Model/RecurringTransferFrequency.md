# RecurringTransferFrequency

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**period_type** | **string** | Typ jednostki okresu czasu / The type of unit of time period | 
**period_value** | **int** | Wartość jednostki okresu czasu / The value of unit of time period | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


