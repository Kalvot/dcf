# RecipientPISTax

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | [**\Swagger\Client\Model\AccountNumber**](AccountNumber.md) |  | 
**name_address** | [**\Swagger\Client\Model\NameAddress**](NameAddress.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


