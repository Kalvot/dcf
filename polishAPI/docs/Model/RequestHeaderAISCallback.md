# RequestHeaderAISCallback

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **string** | Token autoryzacyjny / Authorization token | 
**is_direct_psu** | **bool** | (true/false) Znacznik informujący czy request jest przesłany bezpośrednio przez PSU. Domyślna wartość to false. / Is request sent by PSU directly. false is default value. | [optional] 
**callback_url** | **string** | adres funkcji zwrotnej / callback URL | [optional] 
**api_key** | **string** | API key dla wywołania funkcji zwrotnej / callback API key | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


