# CancelPaymentsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeader**](RequestHeader.md) |  | 
**payment_id** | **string** | Identyfikator płatności / Payment ID | [optional] 
**bundle_id** | **string** | Identyfikator paczki przelewów / Bundle of payments ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


