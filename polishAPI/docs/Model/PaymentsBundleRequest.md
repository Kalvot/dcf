# PaymentsBundleRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderCallback**](RequestHeaderCallback.md) |  | 
**tpp_bundle_id** | **string** | Identyfikator paczki przelewów nadany przez TPP / Bundle of payments identifier set by TPP | 
**transfers_total_amount** | **string** | Łączna kwota wszystkich przelewów z paczki / The total amount of all transfers from bundle | 
**type_of_transfers** | **string** | Typ przelewów, które zostaną zlecone w ramach paczki / The type of transfers that will be initiated through the bundle | 
**domestic_transfers** | [**\Swagger\Client\Model\PaymentDomesticRequestBundled[]**](PaymentDomesticRequestBundled.md) | Lista struktur danych przelewów zwykłych, które zostaną zlecone. Wymagane gdy typeOfTransfers &#x3D; domestic. / The list of data structures of domestic transfers to be initiated. Required when typeOfTransfers &#x3D; domestic. | [optional] 
**eea_transfers** | [**\Swagger\Client\Model\PaymentEEARequestBundled[]**](PaymentEEARequestBundled.md) | Lista struktur danych przelewów zagranicznych SEPA, które zostaną zlecone. Wymagane gdy typeOfTransfers &#x3D; EEA. / The list of data structures of SEPA foreign transfers to be initiated. Required when typeOfTransfers &#x3D; EEA. | [optional] 
**non_eea_transfers** | [**\Swagger\Client\Model\PaymentNonEEARequestBundled[]**](PaymentNonEEARequestBundled.md) | Lista struktur danych przelewów zagranicznych innych niż SEPA, które zostaną zlecone. Wymagane gdy typeOfTransfers &#x3D; nonEEA. / The list of data structures of non SEPA foreign transfers to be initiated. Required when typeOfTransfers &#x3D; nonEEA. | [optional] 
**tax_transfers** | [**\Swagger\Client\Model\PaymentTaxRequestBundled[]**](PaymentTaxRequestBundled.md) | Lista struktur danych przelewów do urzędu podatkowego, które zostaną zlecone. Wymagane gdy typeOfTransfers &#x3D; tax. / The list of data structures of tax transfers to be initiated. Required when typeOfTransfers &#x3D; tax. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


