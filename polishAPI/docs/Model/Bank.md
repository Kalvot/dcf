# Bank

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bic_or_swift** | **string** | Numer BIC/SWIFT Banku / BIC/SWIFT number | [optional] 
**name** | **string** | Nazwa Banku / Bank&#39;s name | [optional] 
**code** | **string** | Kod Banku, dla przelewów zagranicznych / Bank&#39;s code | [optional] 
**country_code** | **string** | Kod kraju 3166-1 / Country code | [optional] 
**address** | [**\Swagger\Client\Model\Address**](Address.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


