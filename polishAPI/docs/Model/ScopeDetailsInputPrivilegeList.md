# ScopeDetailsInputPrivilegeList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | [**\Swagger\Client\Model\AccountNumber**](AccountNumber.md) |  | [optional] 
**ais_accountsget_accounts** | [**\Swagger\Client\Model\PrivilegeAisAspspInSimple**](PrivilegeAisAspspInSimple.md) |  | [optional] 
**aisget_account** | [**\Swagger\Client\Model\PrivilegeAisAspspInSimple**](PrivilegeAisAspspInSimple.md) |  | [optional] 
**aisget_holds** | [**\Swagger\Client\Model\PrivilegeAisAspspIn**](PrivilegeAisAspspIn.md) |  | [optional] 
**aisget_transactions_done** | [**\Swagger\Client\Model\PrivilegeAisAspspIn**](PrivilegeAisAspspIn.md) |  | [optional] 
**aisget_transactions_pending** | [**\Swagger\Client\Model\PrivilegeAisAspspIn**](PrivilegeAisAspspIn.md) |  | [optional] 
**aisget_transactions_rejected** | [**\Swagger\Client\Model\PrivilegeAisAspspIn**](PrivilegeAisAspspIn.md) |  | [optional] 
**aisget_transactions_cancelled** | [**\Swagger\Client\Model\PrivilegeAisAspspIn**](PrivilegeAisAspspIn.md) |  | [optional] 
**aisget_transactions_scheduled** | [**\Swagger\Client\Model\PrivilegeAisAspspIn**](PrivilegeAisAspspIn.md) |  | [optional] 
**aisget_transaction_detail** | [**\Swagger\Client\Model\PrivilegeAisAspspInSimple**](PrivilegeAisAspspInSimple.md) |  | [optional] 
**pisget_payment** | [**\Swagger\Client\Model\PrivilegePayment**](PrivilegePayment.md) |  | [optional] 
**pisget_bundle** | [**\Swagger\Client\Model\PrivilegeBundle**](PrivilegeBundle.md) |  | [optional] 
**pisdomestic** | [**\Swagger\Client\Model\PrivilegeDomesticTransfer**](PrivilegeDomesticTransfer.md) |  | [optional] 
**pis_eea** | [**\Swagger\Client\Model\PrivilegeForeignTransferEEA**](PrivilegeForeignTransferEEA.md) |  | [optional] 
**pisnon_eea** | [**\Swagger\Client\Model\PrivilegeForeignTransferNonEEA**](PrivilegeForeignTransferNonEEA.md) |  | [optional] 
**pistax** | [**\Swagger\Client\Model\PrivilegeTaxTransfer**](PrivilegeTaxTransfer.md) |  | [optional] 
**piscancel_payment** | [**\Swagger\Client\Model\PrivilegeCancelPayment**](PrivilegeCancelPayment.md) |  | [optional] 
**pisbundle** | [**\Swagger\Client\Model\PrivilegeBundleTransfers**](PrivilegeBundleTransfers.md) |  | [optional] 
**pisrecurring** | [**\Swagger\Client\Model\PrivilegeRecurringPayment**](PrivilegeRecurringPayment.md) |  | [optional] 
**pisget_recurring_payment** | [**\Swagger\Client\Model\PrivilegeRecurringPaymentStatus**](PrivilegeRecurringPaymentStatus.md) |  | [optional] 
**piscancel_recurring_payment** | [**\Swagger\Client\Model\PrivilegeCancelRecurringPayment**](PrivilegeCancelRecurringPayment.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


