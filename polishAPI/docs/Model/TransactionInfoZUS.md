# TransactionInfoZUS

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payer_info** | [**\Swagger\Client\Model\SocialSecurityPayor**](SocialSecurityPayor.md) |  | [optional] 
**contribution_type** | **string** | Typ wpłaty / Contribution type | [optional] 
**contribution_id** | **string** | Numer deklaracji / Contribution identifier | [optional] 
**contribution_period** | **string** | Okres deklaracji / Contribution period, Format: MMYYYY | [optional] 
**payment_type_id** | **string** | Identyfikator typu płatności / Payment type ID | [optional] 
**obligation_id** | **string** | Numer tytułu wykonawczego / Obligation identifier or number | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


