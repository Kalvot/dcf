# PrivilegeBundle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scope_usage_limit** | **string** | Rodzaj limitu zgody / Type of limit of usages | [optional] 
**bundle_id** | **string** | Identyfikator paczki płatności / Bundle of payments ID | [optional] 
**tpp_bundle_id** | **string** | Identyfikator paczki przelewów nadany przez TPP. / Bundle of payments identifier set by TPP. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


