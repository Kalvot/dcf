# AccountInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **string** | Numer rachunku / Account number | 
**name_address** | [**\Swagger\Client\Model\NameAddress**](NameAddress.md) |  | 
**account_type** | [**\Swagger\Client\Model\DictionaryItem**](DictionaryItem.md) |  | 
**account_type_name** | **string** | Nazwa typu rachunku (definiowana przez ASPSP) / Account&#39;s type name (defined by ASPSP) | [optional] 
**account_holder_type** | **string** | Rodzaj posiadacza rachunku: osoba fizyczna lub osoba prawna / Account holder type: individual person or corporation | 
**account_name_client** | **string** | Nazwa konta ustawiona przez klienta / Account name set by the client | [optional] 
**currency** | **string** | Waluta rachunku / Currency | 
**available_balance** | **string** | Dostępne środki - po wykonaniu transakcji / Available funds | 
**booking_balance** | **string** | Saldo księgowe rachunku - po wykonaniu transakcji / Book balance of the account | 
**bank** | [**\Swagger\Client\Model\BankAccountInfo**](BankAccountInfo.md) |  | [optional] 
**psu_relations** | [**\Swagger\Client\Model\AccountPsuRelation[]**](AccountPsuRelation.md) | Informacje na temat relacji PSU do rachunku / Description of relations between PSU and an Account | 
**aux_data** | [**\Swagger\Client\Model\Map**](Map.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


