# AddPaymentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**payment_id** | **string** | Identyfiaktor płatności / Payment ID | 
**general_status** | [**\Swagger\Client\Model\PaymentStatus**](PaymentStatus.md) |  | 
**detailed_status** | **string** | Status płatności / Detailed payment status | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


