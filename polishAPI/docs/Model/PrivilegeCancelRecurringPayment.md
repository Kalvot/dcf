# PrivilegeCancelRecurringPayment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scope_usage_limit** | **string** | Rodzaj limitu zgody / Type of limit of usages | [optional] 
**recurring_payment_id** | **string** | Identyfikator płatności cyklicznej nadany przez ASPSP / Recurring payment identifier set by ASPSP | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


