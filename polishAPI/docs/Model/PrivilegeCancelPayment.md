# PrivilegeCancelPayment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scope_usage_limit** | **string** | Rodzaj limitu zgody / Type of limit of usages | [optional] 
**payment_id** | **string** | Identyfikator płatności / Payment ID | [optional] 
**bundle_id** | **string** | Identyfikator paczki przelewów / Bundle of payments ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


