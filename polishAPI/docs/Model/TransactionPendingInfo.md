# TransactionPendingInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction_category** | **string** | Kategoria transakcji uznanie/obciążenie / Transaction category (credit/debit) | 
**initiator** | [**\Swagger\Client\Model\NameAddress**](NameAddress.md) |  | [optional] 
**sender** | [**\Swagger\Client\Model\SenderRecipient**](SenderRecipient.md) |  | [optional] 
**recipient** | [**\Swagger\Client\Model\SenderRecipient**](SenderRecipient.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


