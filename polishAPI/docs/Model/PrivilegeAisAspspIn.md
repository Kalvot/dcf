# PrivilegeAisAspspIn

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scope_usage_limit** | **string** | Rodzaj limitu zgody / Type of limit of usages | [optional] 
**max_allowed_history_long** | **int** | Ilość dni wstecz, liczona od momentu wysłania przez TPP żądania o pobranie historii transakcji rachunku płatnicznego, jaką może obejmować odpowiedź na to żądanie / How much account&#39;s transaction history is allowed to retrieve in days. This value should be used for calculation based on date of sending the request. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


