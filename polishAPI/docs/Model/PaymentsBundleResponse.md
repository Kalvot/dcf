# PaymentsBundleResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**bundle_id** | **string** | Identyfikator paczki przelewów / Bundle of payments identifier | [optional] 
**bundle_status** | **string** | Status paczki przelewów / Bundle of payments status | [optional] 
**bundle_detailed_status** | **string** | Szczegółowy status paczki przelewów / Bundle of payments detailed status | [optional] 
**payments** | [**\Swagger\Client\Model\PaymentInfo[]**](PaymentInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


