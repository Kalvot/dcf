# TransactionInfoCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**card_holder** | **string** | Właściciel karty / Card holder | [optional] 
**card_number** | **string** | Numer karty / Card number | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


