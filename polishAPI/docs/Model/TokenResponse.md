# TokenResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**access_token** | **string** | Wartość wygenerowanego tokena dostępowego | 
**token_type** | **string** | Typ tokena dostępowego. Dozwolona wartość to Bearer. | 
**expires_in** | **string** | Czas ważności tokena dostępowego, po którym zostanie on unieważniony. Wartość wyrażona w sekundach, od momentu wygenerowania odpowiedzi. | 
**refresh_token** | **string** | Wartość wygenerowanego tokena służącego do uzyskania nowego tokena dostępowego bez konieczności ponownej autoryzacji | [optional] 
**scope** | **string** | Typy zgód które uzyskał TPP. Lista identyfikatorów zgodna ze specyfikacją standardu Polish API. | [optional] 
**scope_details** | [**\Swagger\Client\Model\ScopeDetailsOutput**](ScopeDetailsOutput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


