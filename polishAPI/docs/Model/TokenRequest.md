# TokenRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderWithoutTokenAS**](RequestHeaderWithoutTokenAS.md) |  | [optional] 
**grant_type** | **string** | Typ zastosowanej autoryzacji. Jedna z wartości: authorization_code, refresh_token, exchange_token (rozszerzenie standardu OAuth2) | 
**code** | **string** | Kod autoryzacji uzyskany podczas żądania do usługi /authorize OAuth2. Wymagany dla grant_type&#x3D;authorization_code. | [optional] 
**redirect_uri** | **string** | Adres usługi TPP, na które zostanie wykonane przekierowanie po wygenerowaniu tokena dostępowego przez ASPSP. Wymagany dla grant_type&#x3D;authorization_code. | [optional] 
**client_id** | **string** | Identyfikator TPP. Wymagany dla grant_type&#x3D;authorization_code. | [optional] 
**refresh_token** | **string** | Wartość tokena, który służy do uzyskania nowego tokena dostępowego dla tego samego zakresu zgód (scope, scope_details) - w przypadku gdy pierwotny token utraci swoją ważność, lub dla zawężonego zakresu zgód. Wymagany dla grant_type&#x3D;refresh_token. | [optional] 
**exchange_token** | **string** | Wartość tokena, który służy do uzyskania nowego tokena dostępowego dla innego zakresu zgód (scope, scope_details). Wartością tego parametru powinien być token dostępowy ważnej sesji komuikacyjnej z interfejsem XS2A. Wymagany dla grant_type&#x3D;exchange_token. | [optional] 
**scope** | **string** | Typ zgody, o którą prosi TPP. Wybrana wartość z listy dostępnych identyfikatorów zgód, opisanych w specyfikacji standardu Polish API. Wymagany dla grant_type&#x3D;exchange_token. | [optional] 
**scope_details** | [**\Swagger\Client\Model\ScopeDetailsInput**](ScopeDetailsInput.md) |  | [optional] 
**is_user_session** | **bool** | Określa czy dana sesja jest związana z interakcją z PSU – wartości true/false. Rozszerzenie standardu OAuth2. | [optional] 
**user_ip** | **string** | IP przeglądarki użytkownika (informacja na potrzeby fraud detection). Rozszerzenie standardu OAuth2. Wymagany dla is_user_session &#x3D; true. | [optional] 
**user_agent** | **string** | Informacja dotycząca wersji przeglądarki użytkownika (informacja na potrzeby fraud detection). Rozszerzenie standardu OAuth2. Wymagany dla is_user_session &#x3D; true. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


