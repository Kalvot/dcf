# AccountsRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderAISCallback**](RequestHeaderAISCallback.md) |  | 
**type_of_psu_relation** | **string** | Użuwane w celu filtrowania - typ relacji PSU do rachunku / Used for filtering the results - type of relation between PSU and an Account | [optional] 
**page_id** | **string** | Używane w celu stronicowania danych: identyfikator strony, która ma zostać zwrócona w odpowiedzi / Used for paging the results. Identifier of the page to be returned in the response. | [optional] 
**per_page** | **int** | Używane w celu stronicowania danych: wielkość strony danych / Page size (paging info) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


