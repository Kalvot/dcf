# SocialSecurityPayor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nip** | **string** | Numer Identyfikacji Podatkowej / Payor&#39;s Tax Identification Number | [optional] 
**additional_payor_id** | **string** | Dodatkowy numer identyfikacyjny płatnika / Payor&#39;s additional identification number | [optional] 
**additional_payor_id_type** | **string** | Typ dodatkowego identyfikatora płatnika / Payor&#39;s additional identifier type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


