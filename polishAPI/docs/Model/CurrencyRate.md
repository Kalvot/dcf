# CurrencyRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate** | **double** | Kursy przewalutowania / Currency exchange rate | [optional] 
**from_currency** | **string** | Waluta rachunku, kod ISO / from Currency, ISO code | [optional] 
**to_currency** | **string** | Waluta rachunku, kod ISO / to Currency, ISO code | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


