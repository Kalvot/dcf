# RecurringPaymentStatusRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeader**](RequestHeader.md) |  | 
**recurring_payment_id** | **string** | Identyfikator płatności cyklicznej nadany przez ASPSP. Wymagany warunkowo - jeśli TPP otrzymał ten identyfikator od ASPSP. / Recurring payment identifier set by ASPSP. Conditionally required - in case TPP has received this identifier from ASPSP. | [optional] 
**tpp_recurring_payment_id** | **string** | Identyfikator płatności cyklicznej nadany przez TPP. Wymagany warunkowo - jeśli TPP nie otrzymał identyfikatora recurringPaymentId od ASPSP. / Recurring payment identifier set by TPP. Conditionally required - in case TPP has not received recurringPaymentId from ASPSP. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


