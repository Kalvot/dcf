# TransactionPendingInfoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**transactions** | [**\Swagger\Client\Model\TransactionPendingInfo[]**](TransactionPendingInfo.md) |  | [optional] 
**page_info** | [**\Swagger\Client\Model\PageInfo**](PageInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


