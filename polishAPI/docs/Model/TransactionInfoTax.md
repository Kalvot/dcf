# TransactionInfoTax

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payer_info** | [**\Swagger\Client\Model\Payor**](Payor.md) |  | 
**form_code** | **string** | Symbol formularza Urzędu Skarbowego lub Izby Celnej / Tax authority form symbol | 
**period_id** | **string** | Numer okresu. Wymagany warunkowo - w zależności od wartości parametru formCode. / Period number. Required conditionally - depending on formCode parameter value. | [optional] 
**period_type** | **string** | Typ okresu. Wymagany warunkowo - w zależności od wartości parametru formCode. / Period type. Required conditionally - depending on formCode parameter value. | [optional] 
**year** | **int** | Rok okresu. Wymagany warunkowo - w zależności od wartości parametru formCode. / Period year. Required conditionally - depending on formCode parameter value. | [optional] 
**obligation_id** | **string** | Identyfikator zobowiązania, z którego wynika należność podatku np. decyzja, tytuł wykonawczy, postanowienie / Obligation ID | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


