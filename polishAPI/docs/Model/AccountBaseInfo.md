# AccountBaseInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **string** | Numer rachunku (częściowo zamaskowany) / Account number (partly masked) | 
**account_type_name** | **string** | Nazwa typu rachunku (definiowana przez ASPSP) / Account&#39;s type name (defined by ASPSP) | 
**account_type** | [**\Swagger\Client\Model\DictionaryItem**](DictionaryItem.md) |  | 
**psu_relations** | [**\Swagger\Client\Model\AccountPsuRelation[]**](AccountPsuRelation.md) | Informacje na temat relacji PSU do rachunku / Description of relations between PSU and an Account | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


