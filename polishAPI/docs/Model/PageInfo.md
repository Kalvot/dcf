# PageInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previous_page** | **string** | Użyte w celu stronicowania danych: Identyfikator poprzedniej strony rezultatów / Used for paging the results. Identifier of the previous page. | [optional] 
**next_page** | **string** | Użyte w celu stronicowania danych:  Identyfikator następnej strony rezultatów / Used for paging the results. Identifier of the next page. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


