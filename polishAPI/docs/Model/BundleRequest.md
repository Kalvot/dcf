# BundleRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeader**](RequestHeader.md) |  | 
**bundle_id** | **string** | Identyfikator paczki przelewów. Wymagany warunkowo - jeśli TPP otrzymał ten identyfikator od ASPSP. / Bundle of payments identifier.Conditionally required - in case TPP has received this identifier from ASPSP. | [optional] 
**tpp_bundle_id** | **string** | Identyfikator paczki przelewów nadany przez TPP. Wymagany warunkowo - jeśli TPP nie otrzymał identyfikatora bundleId od ASPSP. / Bundle of payments identifier set by TPP. Conditionally required - in case TPP has not received bundleId from ASPSP. | [optional] 
**transactions_included** | **bool** | (true / false) Znacznik oznaczający czy w odpowiedzi mają być uwzględnione statusy transakcji wchodzące w skład paczki / Indicates if statuses of payments from the bundle should be included | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


