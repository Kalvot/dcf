# RecurringPaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderCallback**](RequestHeaderCallback.md) |  | 
**tpp_recurring_payment_id** | **string** | Identyfikator płatności cyklicznej nadany przez TPP / Recurring payment identifier set by TPP | 
**recurrence** | [**\Swagger\Client\Model\RecurringTransferParameters**](RecurringTransferParameters.md) |  | 
**type_of_payment** | **string** | Typ przelewu, który zostanie wykorzystany do zdefiniowania nowej płatności cyklicznej  / The type of payment that is to be used to define new recurring payment | 
**domestic_payment** | [**\Swagger\Client\Model\RecurringDomesticPayment**](RecurringDomesticPayment.md) |  | [optional] 
**eea_payment** | [**\Swagger\Client\Model\RecurringEEAPayment**](RecurringEEAPayment.md) |  | [optional] 
**non_eea_payment** | [**\Swagger\Client\Model\RecurringNonEEAPayment**](RecurringNonEEAPayment.md) |  | [optional] 
**tax_payment** | [**\Swagger\Client\Model\RecurringTaxPayment**](RecurringTaxPayment.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


