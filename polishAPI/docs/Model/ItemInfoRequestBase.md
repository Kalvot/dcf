# ItemInfoRequestBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderAISCallback**](RequestHeaderAISCallback.md) |  | [optional] 
**account_number** | **string** | Numer rachunku / Account number | [optional] 
**item_id_from** | **string** | Element filtru: elementy od podanego identyfikatora (transakcji lub blokady) / Filter element - id of transaction or hold to start from | [optional] 
**transaction_date_from** | [**\DateTime**](\DateTime.md) | Element filtru: data transakcji od, YYYY-MM-DD / Filter element | [optional] 
**transaction_date_to** | [**\DateTime**](\DateTime.md) | Element filtru: data transakcji do, YYYY-MM-DD / Filter element | [optional] 
**booking_date_from** | [**\DateTime**](\DateTime.md) | Element filtru: data księgowania od, YYYY-MM-DD. Ignorowane w przypadku pobierania listy blokad. / Filter element. Ignored during list of holds retrieval. | [optional] 
**booking_date_to** | [**\DateTime**](\DateTime.md) | Element filtru: data księgowania do, YYYY-MM-DD. Ignorowane w przypadku pobierania listy blokad. / Filter element. Ignored during list of holds retrieval. | [optional] 
**min_amount** | **string** | Element filtru: kwota od / Filter element | [optional] 
**max_amount** | **string** | Element filtru: kwota do / Filter element | [optional] 
**page_id** | **string** | Używane w celu stronicowania danych: identyfikator strony, która ma zostać zwrócona w odpowiedzi / Used for paging the results. Identifier of the page to be returned in the response. | [optional] 
**per_page** | **int** | Używane w celu stronicowania danych: wielkość strony danych / Page size (paging info) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


