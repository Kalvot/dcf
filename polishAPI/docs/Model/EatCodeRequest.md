# EatCodeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeaderWithoutTokenCallbackAS**](RequestHeaderWithoutTokenCallbackAS.md) |  | 
**response_type** | **string** | Typ odpowiedzi. Wartość stała: code | 
**eat_code** | **string** | Jednorazowy kod autoryzacyjny wygenerowany przez EAT | 
**eat_type** | [**\Swagger\Client\Model\DictionaryItem**](DictionaryItem.md) |  | [optional] 
**client_id** | **string** | Identyfikator TPP | 
**scope** | **string** | Typ zgody, o którą prosi TPP. Wybrana wartość z listy dostępnych identyfikatorów zgód, opisanych w specyfikacji standardu Polish API. | 
**scope_details** | [**\Swagger\Client\Model\ScopeDetailsInput**](ScopeDetailsInput.md) |  | 
**state** | **string** | Losowa, unikalna w ramach TPP wartość – zabezpieczenie przed atakiem Cross-Site Request Forgery | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


