# RecurringPaymentStatusResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**recurring_payment_id** | **string** | Identyfikator płatności cyklicznej nadany przez APSP / Recurring payment identifier set by ASPSP | 
**tpp_recurring_payment_id** | **string** | Identyfikator płatności cyklicznej nadany przez TPP. / Recurring payment identifier set by TPP. | 
**recurring_payment_status** | **string** | Status płatności cyklicznej / Recurring payment status | [optional] 
**recurring_payment_detailed_status** | **string** | Szczegółowy status płatności cyklicznej / Recurring payment detailed status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


