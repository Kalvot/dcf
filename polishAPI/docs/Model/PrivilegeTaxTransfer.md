# PrivilegeTaxTransfer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scope_usage_limit** | **string** | Rodzaj limitu zgody / Type of limit of usages | [optional] 
**recipient** | [**\Swagger\Client\Model\RecipientPISTax**](RecipientPISTax.md) |  | [optional] 
**sender** | [**\Swagger\Client\Model\SenderPISDomestic**](SenderPISDomestic.md) |  | [optional] 
**transfer_data** | [**\Swagger\Client\Model\TransferDataCurrencyRequiredTax**](TransferDataCurrencyRequiredTax.md) |  | [optional] 
**us_info** | [**\Swagger\Client\Model\TransactionInfoTax**](TransactionInfoTax.md) |  | [optional] 
**delivery_mode** | **string** | Tryb pilności / Urgency mode | [optional] 
**system** | **string** | Droga jaką przelew ma być rozliczony / The way the transfer should be settled | [optional] 
**execution_mode** | **string** | Tryb realizacji płatności. Nadrzędna informacja decydująca o tym w jakim trybie zostanie zrealizowana płatność. / Payment execution mode. The superior information deciding which mode is to be used to execute payment. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


