# RecurringEEAPayment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipient** | [**\Swagger\Client\Model\RecipientPISForeign**](RecipientPISForeign.md) |  | 
**sender** | [**\Swagger\Client\Model\SenderPISForeign**](SenderPISForeign.md) |  | 
**transfer_data** | [**\Swagger\Client\Model\TransferDataCurrencyRequired**](TransferDataCurrencyRequired.md) |  | 
**delivery_mode** | **string** | Tryb pilności / Urgency mode | 
**system** | **string** | Droga jaką przelew ma być rozliczony / The way the transfer should be settled | 
**hold** | **bool** | Czy założyć blokadę (w przypadku np. zlecenia przelewu w dniu wolnym) / Indicates if payment should be holded | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


