# RecipientPISForeign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | [**\Swagger\Client\Model\AccountForeign**](AccountForeign.md) |  | 
**name_address** | [**\Swagger\Client\Model\NameAddress**](NameAddress.md) |  | 
**country_code** | **string** | Kod kraju odbiorcy przelewu, wg normy ISO 3166-1 alfa-3 / Country code of payment recipient, ISO 3166-1 alfa-3 compliant | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


