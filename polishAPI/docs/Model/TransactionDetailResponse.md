# TransactionDetailResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**base_info** | [**\Swagger\Client\Model\TransactionInfo**](TransactionInfo.md) |  | 
**zus_info** | [**\Swagger\Client\Model\TransactionInfoZUS**](TransactionInfoZUS.md) |  | [optional] 
**us_info** | [**\Swagger\Client\Model\TransactionInfoTax**](TransactionInfoTax.md) |  | [optional] 
**card_info** | [**\Swagger\Client\Model\TransactionInfoCard**](TransactionInfoCard.md) |  | [optional] 
**currency_date** | [**\DateTime**](\DateTime.md) | Data kursu waluty, YYYY-MM-DDThh:mm:ss[.mmm] / Date of the currency exchange rate | [optional] 
**transaction_rate** | [**\Swagger\Client\Model\CurrencyRate[]**](CurrencyRate.md) |  | [optional] 
**base_currency** | **string** | Waluta oryginalna transakcji, kod ISO / Currency of the transaction, ISO code | [optional] 
**amount_base_currency** | **string** | Kwota w oryginalnej walucie / Amount of the transaction | [optional] 
**used_payment_instrument_id** | **string** | Unikalny identyfikator instrumentu płatniczego, za którego pomocą wykonano transakcję / Payment Instrument ID | [optional] 
**tpp_transaction_id** | **string** | Unikalny identyfikator transakcji po stronie TPP / Transaction ID (TPP) | [optional] 
**tpp_name** | **string** | Nazwa TPP / TPP name | [optional] 
**rejection_reason** | **string** | Przyczyna odrzucenia / Reason for rejection | [optional] 
**hold_expiration_date** | [**\DateTime**](\DateTime.md) | Data ważności blokady, YYYY-MM-DDThh:mm:ss[.mmm] / Hold expiration date | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


