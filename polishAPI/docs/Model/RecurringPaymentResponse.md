# RecurringPaymentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**recurring_payment_id** | **string** | Identyfikator płatności cyklicznej nadany przez ASPSP / Recurring payment identifier set by ASPSP | [optional] 
**recurrence** | [**\Swagger\Client\Model\RecurringTransferParameters**](RecurringTransferParameters.md) |  | [optional] 
**recurring_payment_status** | **string** | Status płatności cyklicznej / Status of recurring payment | [optional] 
**recurring_payment_detailed_status** | **string** | Szczegółowy status płatności cyklicznej / Recurring payment detailed status | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


