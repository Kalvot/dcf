# Payor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payor_id** | **string** | Identyfikator płatnika / Payor ID | 
**payor_id_type** | **string** | Typ identyfikatora płatnika / Payor ID type | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


