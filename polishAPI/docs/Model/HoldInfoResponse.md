# HoldInfoResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**holds** | [**\Swagger\Client\Model\HoldInfo[]**](HoldInfo.md) |  | [optional] 
**page_info** | [**\Swagger\Client\Model\PageInfo**](PageInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


