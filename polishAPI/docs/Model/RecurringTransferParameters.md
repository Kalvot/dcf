# RecurringTransferParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | [**\DateTime**](\DateTime.md) | Data wykonania pierwszej płatności w definiowanym cyklu. Format: YYYY-MM-DD / First payment execution date (of the defined cycle). | 
**frequency** | [**\Swagger\Client\Model\RecurringTransferFrequency**](RecurringTransferFrequency.md) |  | 
**end_date** | [**\DateTime**](\DateTime.md) | Ostatnia możliwa data wykonania płatności w definiowanym cyklu. Format: YYYY-MM-DD / The last possible payment execution date (of the defined cycle). | [optional] 
**day_off_offset_type** | **string** | Rodzaj przesunięcia, który należy zastosować do wykonania przelewu w przypadku, gdy dzień wolny jest planowaną datą przelewu / Type of offset that should be used for transfer execution in case of day off being the planned date of transfer | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


