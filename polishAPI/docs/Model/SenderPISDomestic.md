# SenderPISDomestic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | [**\Swagger\Client\Model\AccountNumber**](AccountNumber.md) |  | [optional] 
**name_address** | [**\Swagger\Client\Model\NameAddress**](NameAddress.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


