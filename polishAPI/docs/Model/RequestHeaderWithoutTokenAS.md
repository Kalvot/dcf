# RequestHeaderWithoutTokenAS

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**is_company_context** | **bool** | (true / false) Znacznik oznaczający czy żądanie jest wysyłane w kontekście PSU korporacyjnego | [optional] 
**psu_identifier_type** | **string** | Typ identyfikatora PSU, służy do wskazania przez TPP na podstawie jakiej informacji zostanie zidentyfikowany PSU, który będzie podlegał uwierzytelnieniu. Wartość słownikowa. / PSU identifier type, used by TPP to indicate type of information based on which the PSU is to be authenticated. Dictionary value. | [optional] 
**psu_identifier_value** | **string** | Wartość identyfikatora PSU. Wymagany warunkowo - w przypadku gdy została przekazana niepusta wartość typu identyfikatora PSU. / The value of the PSU&#39;s identifier. Required conditionally - in case non empty value of PSU identifier type was passed. | [optional] 
**psu_context_identifier_type** | **string** | Typ identyfikatora kontekstu w jakim występuje PSU. Wartość słownikowa. Wymagany warunkowo - w przypadku wysyłania żądania dla takiego PSU, który może występować w więcej niż jednym kontekście w wybranym ASPSP. / Identifier of context that is used by PSU. Dictionary value. Required conditionally - in case context of the request is used and PSU may use more then one context for the ASPSP. | [optional] 
**psu_context_identifier_value** | **string** | Wartość identyfikatora kontekstu w jakim występuje PSU. Wymagany warunkowo - w przypadku wysyłania żądania dla takiego PSU, który może występować w więcej niż jednym kontekście w wybranym ASPSP. / The value of context that is used by PSU. Required conditionally - in case context of the request is used and PSU may use more then one context for the ASPSP. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


