# PrivilegePayment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scope_usage_limit** | **string** | Rodzaj limitu zgody / Type of limit of usages | [optional] 
**payment_id** | **string** | Identyfikator płatności / Payment ID | [optional] 
**tpp_transaction_id** | **string** | Identyfikator transakcji nadany przez TPP. / Identifier of transaction established by TPP. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


