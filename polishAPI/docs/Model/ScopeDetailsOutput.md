# ScopeDetailsOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**privilege_list** | [**\Swagger\Client\Model\ScopeDetailsOutputPrivilegeList[]**](ScopeDetailsOutputPrivilegeList.md) | The list of structures describing details of consent obtained by TPP. | [optional] 
**consent_id** | **string** | Id of consent | 
**scope_time_limit** | [**\DateTime**](\DateTime.md) | Data ważności zgody / consent valid until, YYYY-MM-DDThh:mm:ss[.mmm] | 
**throttling_policy** | **string** | Throttling policy | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


