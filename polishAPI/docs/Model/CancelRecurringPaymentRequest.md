# CancelRecurringPaymentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request_header** | [**\Swagger\Client\Model\RequestHeader**](RequestHeader.md) |  | 
**recurring_payment_id** | **string** | Identyfikator płatności cyklicznej nadany przez ASPSP / Recurring payment identifier set by ASPSP | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


