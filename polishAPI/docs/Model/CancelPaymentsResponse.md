# CancelPaymentsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**response_header** | [**\Swagger\Client\Model\ResponseHeader**](ResponseHeader.md) |  | 
**payments** | [**\Swagger\Client\Model\PaymentInfo[]**](PaymentInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


