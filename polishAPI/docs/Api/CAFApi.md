# Swagger\Client\CAFApi

All URIs are relative to *https://apiHost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getConfirmationOfFunds**](CAFApi.md#getConfirmationOfFunds) | **POST** /v2_1_1.1/confirmation/v2_1_1.1/getConfirmationOfFunds | Confirmation of the availability of funds


# **getConfirmationOfFunds**
> \Swagger\Client\Model\ConfirmationOfFundsResponse getConfirmationOfFunds($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $confirmation_of_funds_request)

Confirmation of the availability of funds

Confirming the availability on the payers account of the amount necessary to execute the payment transaction, as defined in Art. 65 PSD2.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CAFApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$confirmation_of_funds_request = new \Swagger\Client\Model\ConfirmationOfFundsRequest(); // \Swagger\Client\Model\ConfirmationOfFundsRequest | Object with amount to check

try {
    $result = $apiInstance->getConfirmationOfFunds($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $confirmation_of_funds_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CAFApi->getConfirmationOfFunds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **confirmation_of_funds_request** | [**\Swagger\Client\Model\ConfirmationOfFundsRequest**](../Model/ConfirmationOfFundsRequest.md)| Object with amount to check |

### Return type

[**\Swagger\Client\Model\ConfirmationOfFundsResponse**](../Model/ConfirmationOfFundsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

