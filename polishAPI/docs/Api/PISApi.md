# Swagger\Client\PISApi

All URIs are relative to *https://apiHost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bundle**](PISApi.md#bundle) | **POST** /v2_1_1.1/payments/v2_1_1.1/bundle | Initiate many transfers as bundle
[**cancelPayments**](PISApi.md#cancelPayments) | **POST** /v2_1_1.1/payments/v2_1_1.1/cancelPayments | Cancelation of future dated payment
[**cancelRecurringPayment**](PISApi.md#cancelRecurringPayment) | **POST** /v2_1_1.1/payments/v2_1_1.1/cancelRecurringPayment | Cancelation of recurring payment
[**domestic**](PISApi.md#domestic) | **POST** /v2_1_1.1/payments/v2_1_1.1/domestic | Initiate domestic transfer
[**eEA**](PISApi.md#eEA) | **POST** /v2_1_1.1/payments/v2_1_1.1/EEA | Initiate SEPA foreign transfers
[**getBundle**](PISApi.md#getBundle) | **POST** /v2_1_1.1/payments/v2_1_1.1/getBundle | Get the status of bundle of payments
[**getMultiplePayments**](PISApi.md#getMultiplePayments) | **POST** /v2_1_1.1/payments/v2_1_1.1/getMultiplePayments | Get the status of multiple payments
[**getPayment**](PISApi.md#getPayment) | **POST** /v2_1_1.1/payments/v2_1_1.1/getPayment | Get the status of payment
[**getRecurringPayment**](PISApi.md#getRecurringPayment) | **POST** /v2_1_1.1/payments/v2_1_1.1/getRecurringPayment | Get the status of recurring payment
[**nonEEA**](PISApi.md#nonEEA) | **POST** /v2_1_1.1/payments/v2_1_1.1/nonEEA | Initiate non SEPA foreign transfers
[**recurring**](PISApi.md#recurring) | **POST** /v2_1_1.1/payments/v2_1_1.1/recurring | Defines new recurring payment
[**tax**](PISApi.md#tax) | **POST** /v2_1_1.1/payments/v2_1_1.1/tax | Initiate tax transfer


# **bundle**
> \Swagger\Client\Model\PaymentsBundleResponse bundle($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $bundle_request)

Initiate many transfers as bundle



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$bundle_request = new \Swagger\Client\Model\PaymentsBundleRequest(); // \Swagger\Client\Model\PaymentsBundleRequest | Data for bundle of transfers

try {
    $result = $apiInstance->bundle($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $bundle_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->bundle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **bundle_request** | [**\Swagger\Client\Model\PaymentsBundleRequest**](../Model/PaymentsBundleRequest.md)| Data for bundle of transfers |

### Return type

[**\Swagger\Client\Model\PaymentsBundleResponse**](../Model/PaymentsBundleResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancelPayments**
> \Swagger\Client\Model\CancelPaymentsResponse cancelPayments($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $payment_data)

Cancelation of future dated payment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$payment_data = new \Swagger\Client\Model\CancelPaymentsRequest(); // \Swagger\Client\Model\CancelPaymentsRequest | Payments to be cancelled - identification data

try {
    $result = $apiInstance->cancelPayments($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $payment_data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->cancelPayments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **payment_data** | [**\Swagger\Client\Model\CancelPaymentsRequest**](../Model/CancelPaymentsRequest.md)| Payments to be cancelled - identification data |

### Return type

[**\Swagger\Client\Model\CancelPaymentsResponse**](../Model/CancelPaymentsResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **cancelRecurringPayment**
> \Swagger\Client\Model\CancelRecurringPaymentResponse cancelRecurringPayment($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $recurring_payment_data)

Cancelation of recurring payment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$recurring_payment_data = new \Swagger\Client\Model\CancelRecurringPaymentRequest(); // \Swagger\Client\Model\CancelRecurringPaymentRequest | Recurring payment to be cancelled - identification data

try {
    $result = $apiInstance->cancelRecurringPayment($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $recurring_payment_data);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->cancelRecurringPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **recurring_payment_data** | [**\Swagger\Client\Model\CancelRecurringPaymentRequest**](../Model/CancelRecurringPaymentRequest.md)| Recurring payment to be cancelled - identification data |

### Return type

[**\Swagger\Client\Model\CancelRecurringPaymentResponse**](../Model/CancelRecurringPaymentResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **domestic**
> \Swagger\Client\Model\AddPaymentResponse domestic($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $domestic_request)

Initiate domestic transfer



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$domestic_request = new \Swagger\Client\Model\PaymentDomesticRequest(); // \Swagger\Client\Model\PaymentDomesticRequest | Data for domestic transfer

try {
    $result = $apiInstance->domestic($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $domestic_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->domestic: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **domestic_request** | [**\Swagger\Client\Model\PaymentDomesticRequest**](../Model/PaymentDomesticRequest.md)| Data for domestic transfer |

### Return type

[**\Swagger\Client\Model\AddPaymentResponse**](../Model/AddPaymentResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **eEA**
> \Swagger\Client\Model\AddPaymentResponse eEA($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $eea_request)

Initiate SEPA foreign transfers



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$eea_request = new \Swagger\Client\Model\PaymentEEARequest(); // \Swagger\Client\Model\PaymentEEARequest | Data for SEPA foreign transfer

try {
    $result = $apiInstance->eEA($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $eea_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->eEA: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **eea_request** | [**\Swagger\Client\Model\PaymentEEARequest**](../Model/PaymentEEARequest.md)| Data for SEPA foreign transfer |

### Return type

[**\Swagger\Client\Model\AddPaymentResponse**](../Model/AddPaymentResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBundle**
> \Swagger\Client\Model\BundleResponse getBundle($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $bundle)

Get the status of bundle of payments

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$bundle = new \Swagger\Client\Model\BundleRequest(); // \Swagger\Client\Model\BundleRequest | Bundle ID

try {
    $result = $apiInstance->getBundle($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $bundle);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->getBundle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **bundle** | [**\Swagger\Client\Model\BundleRequest**](../Model/BundleRequest.md)| Bundle ID |

### Return type

[**\Swagger\Client\Model\BundleResponse**](../Model/BundleResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getMultiplePayments**
> \Swagger\Client\Model\PaymentsResponse getMultiplePayments($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $payments)

Get the status of multiple payments



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$payments = new \Swagger\Client\Model\PaymentsRequest(); // \Swagger\Client\Model\PaymentsRequest | Payments ID list

try {
    $result = $apiInstance->getMultiplePayments($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $payments);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->getMultiplePayments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **payments** | [**\Swagger\Client\Model\PaymentsRequest**](../Model/PaymentsRequest.md)| Payments ID list |

### Return type

[**\Swagger\Client\Model\PaymentsResponse**](../Model/PaymentsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPayment**
> \Swagger\Client\Model\GetPaymentResponse getPayment($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $payment)

Get the status of payment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$payment = new \Swagger\Client\Model\PaymentRequest(); // \Swagger\Client\Model\PaymentRequest | Payment ID

try {
    $result = $apiInstance->getPayment($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $payment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->getPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **payment** | [**\Swagger\Client\Model\PaymentRequest**](../Model/PaymentRequest.md)| Payment ID |

### Return type

[**\Swagger\Client\Model\GetPaymentResponse**](../Model/GetPaymentResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRecurringPayment**
> \Swagger\Client\Model\RecurringPaymentStatusResponse getRecurringPayment($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $recurring_payment)

Get the status of recurring payment

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$recurring_payment = new \Swagger\Client\Model\RecurringPaymentStatusRequest(); // \Swagger\Client\Model\RecurringPaymentStatusRequest | Recurring payment identification

try {
    $result = $apiInstance->getRecurringPayment($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $recurring_payment);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->getRecurringPayment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **recurring_payment** | [**\Swagger\Client\Model\RecurringPaymentStatusRequest**](../Model/RecurringPaymentStatusRequest.md)| Recurring payment identification |

### Return type

[**\Swagger\Client\Model\RecurringPaymentStatusResponse**](../Model/RecurringPaymentStatusResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **nonEEA**
> \Swagger\Client\Model\AddPaymentResponse nonEEA($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $non_eea_request)

Initiate non SEPA foreign transfers

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$non_eea_request = new \Swagger\Client\Model\PaymentNonEEARequest(); // \Swagger\Client\Model\PaymentNonEEARequest | Data for non SEPA foreign transfer

try {
    $result = $apiInstance->nonEEA($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $non_eea_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->nonEEA: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **non_eea_request** | [**\Swagger\Client\Model\PaymentNonEEARequest**](../Model/PaymentNonEEARequest.md)| Data for non SEPA foreign transfer |

### Return type

[**\Swagger\Client\Model\AddPaymentResponse**](../Model/AddPaymentResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **recurring**
> \Swagger\Client\Model\RecurringPaymentResponse recurring($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $recurring_request)

Defines new recurring payment



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$recurring_request = new \Swagger\Client\Model\RecurringPaymentRequest(); // \Swagger\Client\Model\RecurringPaymentRequest | Data for recurring payment definition

try {
    $result = $apiInstance->recurring($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $recurring_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->recurring: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **recurring_request** | [**\Swagger\Client\Model\RecurringPaymentRequest**](../Model/RecurringPaymentRequest.md)| Data for recurring payment definition |

### Return type

[**\Swagger\Client\Model\RecurringPaymentResponse**](../Model/RecurringPaymentResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **tax**
> \Swagger\Client\Model\AddPaymentResponse tax($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $tax_request)

Initiate tax transfer



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\PISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$tax_request = new \Swagger\Client\Model\PaymentTaxRequest(); // \Swagger\Client\Model\PaymentTaxRequest | Data for tax transfer

try {
    $result = $apiInstance->tax($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $tax_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PISApi->tax: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **tax_request** | [**\Swagger\Client\Model\PaymentTaxRequest**](../Model/PaymentTaxRequest.md)| Data for tax transfer |

### Return type

[**\Swagger\Client\Model\AddPaymentResponse**](../Model/AddPaymentResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

