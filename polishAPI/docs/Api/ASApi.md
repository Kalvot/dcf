# Swagger\Client\ASApi

All URIs are relative to *https://apiHost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorize**](ASApi.md#authorize) | **POST** /v2_1_1.1/auth/v2_1_1.1/authorize | Requests OAuth2 authorization code
[**authorizeExt**](ASApi.md#authorizeExt) | **POST** /v2_1_1.1/auth/v2_1_1.1/authorizeExt | Requests OAuth2 authorization code based on One-time authorization code issued by External Authorization Tool
[**token**](ASApi.md#token) | **POST** /v2_1_1.1/auth/v2_1_1.1/token | Requests OAuth2 access token value


# **authorize**
> \Swagger\Client\Model\AuthorizeResponse authorize($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $authorize_request)

Requests OAuth2 authorization code

Requests OAuth2 authorization code

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ASApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$authorize_request = new \Swagger\Client\Model\AuthorizeRequest(); // \Swagger\Client\Model\AuthorizeRequest | Data for OAuth2 Authorization Code Request

try {
    $result = $apiInstance->authorize($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $authorize_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ASApi->authorize: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **authorize_request** | [**\Swagger\Client\Model\AuthorizeRequest**](../Model/AuthorizeRequest.md)| Data for OAuth2 Authorization Code Request |

### Return type

[**\Swagger\Client\Model\AuthorizeResponse**](../Model/AuthorizeResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **authorizeExt**
> authorizeExt($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $eat_code_request)

Requests OAuth2 authorization code based on One-time authorization code issued by External Authorization Tool

Requests OAuth2 authorization code based One-time authorization code issued by External Authorization Tool. Authorization code will be delivered to TPP as callback request from ASPSP if PSU authentication is confirmed by EAT. Callback function must provide similar notification also in case of unsuccessful authentication or its abandonment.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ASApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$eat_code_request = new \Swagger\Client\Model\EatCodeRequest(); // \Swagger\Client\Model\EatCodeRequest | Data for OAuth2 Authorization Code Request extended for EAT based authentication and callback response

try {
    $apiInstance->authorizeExt($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $eat_code_request);
} catch (Exception $e) {
    echo 'Exception when calling ASApi->authorizeExt: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **eat_code_request** | [**\Swagger\Client\Model\EatCodeRequest**](../Model/EatCodeRequest.md)| Data for OAuth2 Authorization Code Request extended for EAT based authentication and callback response |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **token**
> \Swagger\Client\Model\TokenResponse token($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $token_request)

Requests OAuth2 access token value

Requests OAuth2 access token value

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ASApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$token_request = new \Swagger\Client\Model\TokenRequest(); // \Swagger\Client\Model\TokenRequest | Data for OAuth2 Access Token Request

try {
    $result = $apiInstance->token($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $token_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ASApi->token: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **token_request** | [**\Swagger\Client\Model\TokenRequest**](../Model/TokenRequest.md)| Data for OAuth2 Access Token Request |

### Return type

[**\Swagger\Client\Model\TokenResponse**](../Model/TokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

