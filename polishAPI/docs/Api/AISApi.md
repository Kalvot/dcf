# Swagger\Client\AISApi

All URIs are relative to *https://apiHost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteConsent**](AISApi.md#deleteConsent) | **POST** /v2_1_1.1/accounts/v2_1_1.1/deleteConsent | Removes consent
[**getAccount**](AISApi.md#getAccount) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getAccount | Get detailed information about user payment account
[**getAccounts**](AISApi.md#getAccounts) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getAccounts | Get information about all user&#39;s payment account
[**getHolds**](AISApi.md#getHolds) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getHolds | Get list of user&#39;s holded operations
[**getTransactionDetail**](AISApi.md#getTransactionDetail) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getTransactionDetail | Get detailed information about user&#39;s single transaction
[**getTransactionsCancelled**](AISApi.md#getTransactionsCancelled) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getTransactionsCancelled | Get list of user cancelled transactions
[**getTransactionsDone**](AISApi.md#getTransactionsDone) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getTransactionsDone | Get list of user done transactions
[**getTransactionsPending**](AISApi.md#getTransactionsPending) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getTransactionsPending | Get list of user&#39;s pending transactions
[**getTransactionsRejected**](AISApi.md#getTransactionsRejected) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getTransactionsRejected | Get list of user&#39;s rejected transactions
[**getTransactionsScheduled**](AISApi.md#getTransactionsScheduled) | **POST** /v2_1_1.1/accounts/v2_1_1.1/getTransactionsScheduled | Get list of user scheduled transactions


# **deleteConsent**
> deleteConsent($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $delete_consent_request)

Removes consent

Removes consent

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$delete_consent_request = new \Swagger\Client\Model\DeleteConsentRequest(); // \Swagger\Client\Model\DeleteConsentRequest | Data for delete Consent Request

try {
    $apiInstance->deleteConsent($accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $delete_consent_request);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->deleteConsent: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **delete_consent_request** | [**\Swagger\Client\Model\DeleteConsentRequest**](../Model/DeleteConsentRequest.md)| Data for delete Consent Request |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAccount**
> \Swagger\Client\Model\AccountResponse getAccount($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_account_request)

Get detailed information about user payment account

User identification based on access token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_account_request = new \Swagger\Client\Model\AccountInfoRequest(); // \Swagger\Client\Model\AccountInfoRequest | Data for Account Request

try {
    $result = $apiInstance->getAccount($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_account_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getAccount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_account_request** | [**\Swagger\Client\Model\AccountInfoRequest**](../Model/AccountInfoRequest.md)| Data for Account Request |

### Return type

[**\Swagger\Client\Model\AccountResponse**](../Model/AccountResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAccounts**
> \Swagger\Client\Model\AccountsResponse getAccounts($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_accounts_request)

Get information about all user's payment account

User identification based on access token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_accounts_request = new \Swagger\Client\Model\AccountsRequest(); // \Swagger\Client\Model\AccountsRequest | Data for Accounts Request

try {
    $result = $apiInstance->getAccounts($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_accounts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getAccounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_accounts_request** | [**\Swagger\Client\Model\AccountsRequest**](../Model/AccountsRequest.md)| Data for Accounts Request |

### Return type

[**\Swagger\Client\Model\AccountsResponse**](../Model/AccountsResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getHolds**
> \Swagger\Client\Model\HoldInfoResponse getHolds($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_holds_request)

Get list of user's holded operations



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_holds_request = new \Swagger\Client\Model\HoldRequest(); // \Swagger\Client\Model\HoldRequest | Data for hold Request

try {
    $result = $apiInstance->getHolds($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_holds_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getHolds: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_holds_request** | [**\Swagger\Client\Model\HoldRequest**](../Model/HoldRequest.md)| Data for hold Request |

### Return type

[**\Swagger\Client\Model\HoldInfoResponse**](../Model/HoldInfoResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionDetail**
> \Swagger\Client\Model\TransactionDetailResponse getTransactionDetail($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transation_detail_request)

Get detailed information about user's single transaction



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_transation_detail_request = new \Swagger\Client\Model\TransactionDetailRequest(); // \Swagger\Client\Model\TransactionDetailRequest | Data for transation detail Request

try {
    $result = $apiInstance->getTransactionDetail($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transation_detail_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getTransactionDetail: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_transation_detail_request** | [**\Swagger\Client\Model\TransactionDetailRequest**](../Model/TransactionDetailRequest.md)| Data for transation detail Request |

### Return type

[**\Swagger\Client\Model\TransactionDetailResponse**](../Model/TransactionDetailResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionsCancelled**
> \Swagger\Client\Model\TransactionsCancelledInfoResponse getTransactionsCancelled($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_cancelled_request)

Get list of user cancelled transactions



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_transactions_cancelled_request = new \Swagger\Client\Model\TransactionInfoRequest(); // \Swagger\Client\Model\TransactionInfoRequest | Data for Transactions Cancelled Request

try {
    $result = $apiInstance->getTransactionsCancelled($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_cancelled_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getTransactionsCancelled: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_transactions_cancelled_request** | [**\Swagger\Client\Model\TransactionInfoRequest**](../Model/TransactionInfoRequest.md)| Data for Transactions Cancelled Request |

### Return type

[**\Swagger\Client\Model\TransactionsCancelledInfoResponse**](../Model/TransactionsCancelledInfoResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionsDone**
> \Swagger\Client\Model\TransactionsDoneInfoResponse getTransactionsDone($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_done_request)

Get list of user done transactions



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_transactions_done_request = new \Swagger\Client\Model\TransactionInfoRequest(); // \Swagger\Client\Model\TransactionInfoRequest | Data for Transactions Done Request

try {
    $result = $apiInstance->getTransactionsDone($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_done_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getTransactionsDone: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_transactions_done_request** | [**\Swagger\Client\Model\TransactionInfoRequest**](../Model/TransactionInfoRequest.md)| Data for Transactions Done Request |

### Return type

[**\Swagger\Client\Model\TransactionsDoneInfoResponse**](../Model/TransactionsDoneInfoResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionsPending**
> \Swagger\Client\Model\TransactionPendingInfoResponse getTransactionsPending($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_pending_request)

Get list of user's pending transactions



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_transactions_pending_request = new \Swagger\Client\Model\TransactionInfoRequest(); // \Swagger\Client\Model\TransactionInfoRequest | Data for Transactions Pending Request

try {
    $result = $apiInstance->getTransactionsPending($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_pending_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getTransactionsPending: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_transactions_pending_request** | [**\Swagger\Client\Model\TransactionInfoRequest**](../Model/TransactionInfoRequest.md)| Data for Transactions Pending Request |

### Return type

[**\Swagger\Client\Model\TransactionPendingInfoResponse**](../Model/TransactionPendingInfoResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionsRejected**
> \Swagger\Client\Model\TransactionRejectedInfoResponse getTransactionsRejected($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_rejected_request)

Get list of user's rejected transactions



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_transactions_rejected_request = new \Swagger\Client\Model\TransactionInfoRequest(); // \Swagger\Client\Model\TransactionInfoRequest | Data for Transactions Rejected Request

try {
    $result = $apiInstance->getTransactionsRejected($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_rejected_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getTransactionsRejected: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_transactions_rejected_request** | [**\Swagger\Client\Model\TransactionInfoRequest**](../Model/TransactionInfoRequest.md)| Data for Transactions Rejected Request |

### Return type

[**\Swagger\Client\Model\TransactionRejectedInfoResponse**](../Model/TransactionRejectedInfoResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getTransactionsScheduled**
> \Swagger\Client\Model\TransactionsScheduledInfoResponse getTransactionsScheduled($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_scheduled_request)

Get list of user scheduled transactions



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: xs2a_auth_aspsp
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');
// Configure OAuth2 access token for authorization: xs2a_auth_decoupled
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\AISApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authorization = "authorization_example"; // string | The value of the Authorization header should consist of 'type' + 'credentials', where for the approach using the 'type' token should be 'Bearer'.
$accept_encoding = "accept_encoding_example"; // string | Gzip, deflate
$accept_language = "accept_language_example"; // string | Prefered language of response
$accept_charset = "accept_charset_example"; // string | UTF-8
$x_jws_signature = "x_jws_signature_example"; // string | Detached JWS signature of the body of the payload
$x_request_id = "x_request_id_example"; // string | Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload.
$get_transactions_scheduled_request = new \Swagger\Client\Model\TransactionInfoRequest(); // \Swagger\Client\Model\TransactionInfoRequest | Data for Transactions Scheduled Request

try {
    $result = $apiInstance->getTransactionsScheduled($authorization, $accept_encoding, $accept_language, $accept_charset, $x_jws_signature, $x_request_id, $get_transactions_scheduled_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AISApi->getTransactionsScheduled: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **string**| The value of the Authorization header should consist of &#39;type&#39; + &#39;credentials&#39;, where for the approach using the &#39;type&#39; token should be &#39;Bearer&#39;. |
 **accept_encoding** | **string**| Gzip, deflate |
 **accept_language** | **string**| Prefered language of response |
 **accept_charset** | **string**| UTF-8 |
 **x_jws_signature** | **string**| Detached JWS signature of the body of the payload |
 **x_request_id** | **string**| Identyfikator żądania w formacie UUID (Wariant 1, Wersja 1), zgodnym ze standardem RFC 4122, nadawany przez TPP. Wartość musi być zgodna z parametrem requestId przekazywanym w ciele każdego żądania. / Request identifier using UUID format (Variant 1, Version 1), described in RFC 4122 standard, set by TPP. Value of this header must be the same as for the requestId param passed inside request payload. |
 **get_transactions_scheduled_request** | [**\Swagger\Client\Model\TransactionInfoRequest**](../Model/TransactionInfoRequest.md)| Data for Transactions Scheduled Request |

### Return type

[**\Swagger\Client\Model\TransactionsScheduledInfoResponse**](../Model/TransactionsScheduledInfoResponse.md)

### Authorization

[xs2a_auth_aspsp](../../README.md#xs2a_auth_aspsp), [xs2a_auth_decoupled](../../README.md#xs2a_auth_decoupled)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

