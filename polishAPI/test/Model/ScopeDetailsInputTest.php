<?php
/**
 * ScopeDetailsInputTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Polish API
 *
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.5
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * ScopeDetailsInputTest Class Doc Comment
 *
 * @category    Class
 * @description Zakres, limity, parametry i czas ważności zgód, o które prosi TPP. / Scope, limits, parameters and expiration time of consents requested by TPP
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ScopeDetailsInputTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ScopeDetailsInput"
     */
    public function testScopeDetailsInput()
    {
    }

    /**
     * Test attribute "privilege_list"
     */
    public function testPropertyPrivilegeList()
    {
    }

    /**
     * Test attribute "scope_group_type"
     */
    public function testPropertyScopeGroupType()
    {
    }

    /**
     * Test attribute "consent_id"
     */
    public function testPropertyConsentId()
    {
    }

    /**
     * Test attribute "scope_time_limit"
     */
    public function testPropertyScopeTimeLimit()
    {
    }

    /**
     * Test attribute "throttling_policy"
     */
    public function testPropertyThrottlingPolicy()
    {
    }
}
