<?php
/**
 * AccountBaseInfoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Polish API
 *
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.5
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * AccountBaseInfoTest Class Doc Comment
 *
 * @category    Class
 * @description Klasa informacji o koncie / Account Information Class
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AccountBaseInfoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AccountBaseInfo"
     */
    public function testAccountBaseInfo()
    {
    }

    /**
     * Test attribute "account_number"
     */
    public function testPropertyAccountNumber()
    {
    }

    /**
     * Test attribute "account_type_name"
     */
    public function testPropertyAccountTypeName()
    {
    }

    /**
     * Test attribute "account_type"
     */
    public function testPropertyAccountType()
    {
    }

    /**
     * Test attribute "psu_relations"
     */
    public function testPropertyPsuRelations()
    {
    }
}
