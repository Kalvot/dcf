<?php
/**
 * ScopeDetailsInputPrivilegeListTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Polish API
 *
 * Interface specification for services provided by third parties based on access to payment accounts. Prepared by the Polish Bank Association and its affiliates
 *
 * OpenAPI spec version: 2_1_2
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.5
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * ScopeDetailsInputPrivilegeListTest Class Doc Comment
 *
 * @category    Class
 * @description Pełna lista uprawnień będących przedmiotem zapytania o zgodę PSU / The list of all privileges that are the subject of the request for PSU&#39;s consent
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ScopeDetailsInputPrivilegeListTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ScopeDetailsInputPrivilegeList"
     */
    public function testScopeDetailsInputPrivilegeList()
    {
    }

    /**
     * Test attribute "account_number"
     */
    public function testPropertyAccountNumber()
    {
    }

    /**
     * Test attribute "ais_accountsget_accounts"
     */
    public function testPropertyAisAccountsgetAccounts()
    {
    }

    /**
     * Test attribute "aisget_account"
     */
    public function testPropertyAisgetAccount()
    {
    }

    /**
     * Test attribute "aisget_holds"
     */
    public function testPropertyAisgetHolds()
    {
    }

    /**
     * Test attribute "aisget_transactions_done"
     */
    public function testPropertyAisgetTransactionsDone()
    {
    }

    /**
     * Test attribute "aisget_transactions_pending"
     */
    public function testPropertyAisgetTransactionsPending()
    {
    }

    /**
     * Test attribute "aisget_transactions_rejected"
     */
    public function testPropertyAisgetTransactionsRejected()
    {
    }

    /**
     * Test attribute "aisget_transactions_cancelled"
     */
    public function testPropertyAisgetTransactionsCancelled()
    {
    }

    /**
     * Test attribute "aisget_transactions_scheduled"
     */
    public function testPropertyAisgetTransactionsScheduled()
    {
    }

    /**
     * Test attribute "aisget_transaction_detail"
     */
    public function testPropertyAisgetTransactionDetail()
    {
    }

    /**
     * Test attribute "pisget_payment"
     */
    public function testPropertyPisgetPayment()
    {
    }

    /**
     * Test attribute "pisget_bundle"
     */
    public function testPropertyPisgetBundle()
    {
    }

    /**
     * Test attribute "pisdomestic"
     */
    public function testPropertyPisdomestic()
    {
    }

    /**
     * Test attribute "pis_eea"
     */
    public function testPropertyPisEea()
    {
    }

    /**
     * Test attribute "pisnon_eea"
     */
    public function testPropertyPisnonEea()
    {
    }

    /**
     * Test attribute "pistax"
     */
    public function testPropertyPistax()
    {
    }

    /**
     * Test attribute "piscancel_payment"
     */
    public function testPropertyPiscancelPayment()
    {
    }

    /**
     * Test attribute "pisbundle"
     */
    public function testPropertyPisbundle()
    {
    }

    /**
     * Test attribute "pisrecurring"
     */
    public function testPropertyPisrecurring()
    {
    }

    /**
     * Test attribute "pisget_recurring_payment"
     */
    public function testPropertyPisgetRecurringPayment()
    {
    }

    /**
     * Test attribute "piscancel_recurring_payment"
     */
    public function testPropertyPiscancelRecurringPayment()
    {
    }
}
